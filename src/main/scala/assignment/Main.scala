package assignment

import assignment.Utilities.daysAndClockFormat

object Main extends App {

  lazy val MinPebbles = 27
  lazy val MaxPebbles = 127

  lazy val parser = new scopt.OptionParser[Arguments]("Pebble Clock") {
    head("Pebble clock simulator")

    note(
      "Program simulates a pebble clock mechanism with 4-pebble-track for single minutes, " +
        "11-pebble-track for 5 minutes and 11-pebble-track for hours with one additional fixed pebble. " +
        "Possible clock reading is from 01:00 to 12:59.\n"
    )
    note("Program supports two modes:")
    note(
      "1) Computes number of days it takes to repeat initial pebble order. Pass <pebbles> argument only."
    )
    note(
      "2) Computes state of all tracks after specified time in minutes in json. Pass both <pebbles> and <minutes> arguments.\n"
    )

    arg[Int]("<pebbles>")
      .required()
      .text("Pebbles to deposit to the queue track.")
      .action((pebbles, arguments) => arguments.copy(optPebbles = Some(pebbles)))
      .validate(
        pebbles =>
          if (pebbles >= MinPebbles && pebbles <= MaxPebbles) Right()
          else Left(s"Pebbles to deposit to the queue track must be in range from $MinPebbles to $MaxPebbles.")
      )

    arg[Int]("<minutes>")
      .optional()
      .text("Minutes to iterate the clock.")
      .action((minutes, arguments) => arguments.copy(optMinutes = Some(minutes)))
      .validate(
        minutes =>
          if (minutes > 0) Right()
          else Left("Minutes to iterate the clock must be a positive value.")
      )
  }

  def assignmentEmptyPebbleClock: PebbleClock =
    new PebbleClock(
      List(
        new ClockTrack("Min", 4, minuteMultiplier = 1),
        new ClockTrack("FiveMin", 11, minuteMultiplier = 5),
        new ClockTrack("Hour", 11, minuteMultiplier = 60, fixedPebbles = 1)
      ),
      new QueueTrack("Main", MaxPebbles)
    )

  def resultForArguments(PebbleClock: PebbleClock, arguments: Arguments): Either[String, String] = {
    arguments match {
      case Arguments(Some(pebbles), None) =>
        PebbleClock.deposit(1 to pebbles: _*)
        val (days, hours, minutes) = PebbleClock.daysHoursMinutesUntilOrderingRepeats()
        Right(s"$pebbles pebbles cycle after ${daysAndClockFormat(days, hours, minutes)}.")
      case Arguments(Some(pebbles), Some(minutes)) =>
        PebbleClock.deposit(1 to pebbles: _*)
        PebbleClock.iterate(minutes)
        Right(PebbleClock.jsonTracksState)
      case _ => Left("Pebbles to deposit field is required.")
    }
  }

  case class Arguments(optPebbles: Option[Int] = None, optMinutes: Option[Int] = None)

  val optArguments = parser.parse(args, Arguments())

  optArguments.map { argument =>
    resultForArguments(assignmentEmptyPebbleClock, argument) match {
      case Right(result) => println(result)
      case Left(error) => System.err.println(error)
    }
  }

}
