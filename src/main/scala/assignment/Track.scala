package assignment

sealed trait Track {

  def name: String

  val capacity: Int

  def pebbles: Seq[Int]
  def pebblesOrderForOutput: Seq[Int]
  def pebblesCount: Int

  def deposit(pebble: Int): Unit

  def areInTheSameOrder(otherPebbles: Vector[Int]): Boolean = {
    if (otherPebbles.size == pebblesCount) {
      var index = 0
      pebbles.forall { pebble =>
        val otherPebble = otherPebbles(index)
        index += 1
        pebble == otherPebble
      }
    } else {
      false
    }
  }

}

class QueueTrack(val name: String, val capacity: Int) extends Track {

  private val queue: Array[Int] = Array.fill(capacity)(0)
  private var qHeadIndex: Int   = 0
  private var qTailIndex: Int   = 0

  private var pebblesCounter: Int = 0

  override def pebbles: Seq[Int] =
    if (qHeadIndex <= qTailIndex && pebblesCounter != capacity) {
      queue.slice(qHeadIndex, qTailIndex)
    } else {
      queue.slice(qHeadIndex, capacity) ++ queue.slice(0, qTailIndex)
    }
  override def pebblesOrderForOutput: Seq[Int] = pebbles
  override def pebblesCount: Int               = pebblesCounter

  override def deposit(pebble: Int): Unit = {
    queue(qTailIndex) = pebble
    qTailIndex = if (qTailIndex == capacity - 1) 0 else qTailIndex + 1
    pebblesCounter += 1
  }

  def dequeue(): Int = {
    val value = queue(qHeadIndex)
    qHeadIndex = if (qHeadIndex == capacity - 1) 0 else qHeadIndex + 1
    pebblesCounter -= 1
    value
  }

}

class ClockTrack(
    val name: String,
    val capacity: Int,
    minuteMultiplier: Int = 1,
    fixedPebbles: Int = 0
) extends Track {

  private var stack: List[Int]  = Nil
  private var pebblesCounter: Int = 0

  def isFull: Boolean = capacity == pebblesCounter

  override def pebbles: Seq[Int]               = stack
  override def pebblesOrderForOutput: Seq[Int] = stack.reverse
  override def pebblesCount: Int               = pebblesCounter
  def pebblesCountIncludingFixed: Int          = pebblesCounter + fixedPebbles

  override def deposit(pebble: Int): Unit = {
    stack = pebble :: stack
    pebblesCounter += 1
  }

  def tilt(): Seq[Int] = {
    val result = stack
    stack = Nil
    pebblesCounter = 0
    result
  }

  def sumMinutes: Int = pebblesCountIncludingFixed * minuteMultiplier

}
