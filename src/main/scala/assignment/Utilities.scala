package assignment

object Utilities {

  def summedMinutesToDaysHoursMinutes(summedMinutes: Int): (Int, Int, Int) = {
    val (days, remMinutes) = (summedMinutes / (24 * 60), summedMinutes % (24 * 60))
    val (hours, minutes)   = (remMinutes / 60, remMinutes              % 60)
    (days, hours, minutes)
  }

  def daysAndClockFormat(days: Int = 0, hours: Int = 0, minutes: Int = 0): String = {
    val daysPrefix   = if (days == 1) s"$days day" else if (days > 1) s"$days days" else ""
    val clockPostfix = if (hours > 0 || minutes > 0) clockFormat(hours, minutes) else ""
    if (daysPrefix.nonEmpty && clockPostfix.nonEmpty) {
      s"$daysPrefix, $clockPostfix"
    } else {
      val result = daysPrefix ++ clockPostfix
      if (result.nonEmpty) result else clockFormat(hours, minutes)
    }
  }

  def clockFormat(hours: Int = 0, minutes: Int = 0): String = {
    "%02d".format(hours) ++ ":" ++ "%02d".format(minutes)
  }

}
