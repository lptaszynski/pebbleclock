package assignment

import assignment.Utilities._
import io.circe.Json
import io.circe.syntax._

import scala.annotation.tailrec

class PebbleClock(clockTracks: List[ClockTrack], queueTrack: QueueTrack) {

  def deposit(pebbles: Int*): Unit = pebbles.foreach(queueTrack.deposit)

  def daysHoursMinutesUntilOrderingRepeats(): (Int, Int, Int) = {
    val minutesPassed = iterateUntilOrderingRepeats()
    summedMinutesToDaysHoursMinutes(minutesPassed)
  }

  /** Iterates the clock until pebbles are in the same positions.
    *  @return number of iterations (minutes) it took
    */
  private def iterateUntilOrderingRepeats(): Int = {
    val cTracksOrdering = clockTracks.map(_.pebbles.toVector).toList
    val qTrackOrdering  = queueTrack.pebbles.toVector

    iterate // initial iteration
    var orderNotRepeatedYet = true
    var iterationsPassed    = 1
    while (orderNotRepeatedYet) {
      if (areInTheSameOrder(cTracksOrdering, qTrackOrdering)) {
        orderNotRepeatedYet = false
      } else {
        iterate
        iterationsPassed += 1
      }
    }
    iterationsPassed
  }

  def iterate(minutes: Int): Unit = {
    var tmpMinutes = minutes
    while (tmpMinutes > 0) {
      iterate
      tmpMinutes -= 1
    }
  }

  def iterate: Unit = {
    val pebble = queueTrack.dequeue
    deposit(pebble)
  }

  def areInTheSameOrder(
      clockTracksOrdering: List[Vector[Int]],
      queueTrackOrdering: Vector[Int]
  ): Boolean = {

    @tailrec
    def areClockTracksInTheSameOrder(
        clockTracks: List[ClockTrack],
        clockTrackOrdering: List[Vector[Int]]
    ): Boolean = {
      if (clockTracks.isEmpty) {
        true
      } else {
        if (clockTracks.head.areInTheSameOrder(clockTrackOrdering.head)) {
          areClockTracksInTheSameOrder(clockTracks.tail, clockTrackOrdering.tail)
        } else {
          false
        }
      }
    }
    areClockTracksInTheSameOrder(clockTracks, clockTracksOrdering) && queueTrack.areInTheSameOrder(queueTrackOrdering)
  }

  private def deposit(pebble: Int): Unit = {

    @tailrec
    def depositRec(clockTracksToPass: List[ClockTrack]): Unit = {
      clockTracksToPass match {
        case Nil => queueTrack.deposit(pebble)
        case clockTracks =>
          if (clockTracks.head.isFull) {
            clockTracks.head.tilt().foreach(queueTrack.deposit)
            depositRec(clockTracks.tail)
          } else {
            clockTracks.head.deposit(pebble)
          }
      }
    }
    depositRec(clockTracks)
  }

  def formatClock: String = {
    val (days, hours, minutes) = summedMinutesToDaysHoursMinutes(sumMinutes)
    daysAndClockFormat(days, hours, minutes)
  }

  private def sumMinutes: Int = clockTracks.map(_.sumMinutes).sum

  def jsonTracksState: String = {
    val namePebblesPairs     = allTracks.map(track => track.name -> track.pebblesOrderForOutput)
    val namePebblesJsonPairs = namePebblesPairs.map { case (name, pebbles) => name -> pebbles.asJson }
    val jsonNamePebblesPairs = Json.obj(namePebblesJsonPairs: _*)
    jsonNamePebblesPairs.noSpaces
  }

  def allTracks: Seq[Track] = clockTracks :+ queueTrack

}
