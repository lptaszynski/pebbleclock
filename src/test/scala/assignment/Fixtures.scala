package assignment

object Fixtures {
  val createPebbleClock = () => Main.assignmentEmptyPebbleClock

  val testCase1 = FirstModeTestCase(createPebbleClock, 30, 15)
  val testCase2 = FirstModeTestCase(createPebbleClock, 45, 378)
  val testCase3 = SecondModeTestCase(
    createPebbleClock,
    30,
    325,
    "{\"Min\":[],\"FiveMin\":[22,13,25,3,7],\"Hour\":[6,12,17,4,15],\"Main\":[11,5,26,18,2,30,19,8,24,10,29,20,16,21,28,1,23,14,27,9]}"
  )

}
