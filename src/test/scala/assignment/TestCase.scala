package assignment

import assignment.Main.Arguments

sealed trait TestCase {

  def createPebbleClock: () => PebbleClock

  def args: Array[String]
  def arguments: Arguments

  def expectedOutput: String

}

case class FirstModeTestCase(createPebbleClock: () => PebbleClock, pebbles: Int, expectedDays: Int)
    extends TestCase {
  override def args: Array[String]    = Array(pebbles.toString)
  override def arguments: Arguments   = Arguments(Some(pebbles))
  override def expectedOutput: String = s"$pebbles pebbles cycle after $expectedDays days."
}

case class SecondModeTestCase(
                               createPebbleClock: () => PebbleClock,
                               pebbles: Int,
                               minutes: Int,
                               expectedOutput: String
) extends TestCase {
  override def args: Array[String]  = Array(pebbles.toString, minutes.toString)
  override def arguments: Arguments = Arguments(Some(pebbles), Some(minutes))
}
