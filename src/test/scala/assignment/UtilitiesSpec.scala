package assignment

import assignment.Utilities._
import org.scalatest._

class UtilitiesSpec extends FlatSpec with Matchers {

  "Utilities" should "have a valid clock format method " in {
    clockFormat(0, 0) shouldBe "00:00"
    clockFormat(12, 30) shouldBe "12:30"
  }

  it should "have a valid days and clock format method" in {
    daysAndClockFormat() shouldBe "00:00"
    daysAndClockFormat(minutes = 10) shouldBe "00:10"
    daysAndClockFormat(hours = 12) shouldBe "12:00"
    daysAndClockFormat(days = 378) shouldBe "378 days"
    daysAndClockFormat(days = 378, hours = 12) shouldBe "378 days, 12:00"
    daysAndClockFormat(days = 1, hours = 15, minutes = 10) shouldBe "1 day, 15:10"
  }

  it should "have a valid summed minutes to (days, hours, minutes) conversion method" in {
    summedMinutesToDaysHoursMinutes(1) shouldBe (0, 0, 1)
    summedMinutesToDaysHoursMinutes(60 + 1) shouldBe (0, 1, 1)
    summedMinutesToDaysHoursMinutes(2 * 60 + 2) shouldBe (0, 2, 2)
    summedMinutesToDaysHoursMinutes(25 * 60 + 30) shouldBe (1, 1, 30)
  }

}
