package assignment

import assignment.Fixtures._
import assignment.Main.{Arguments, parser}
import org.scalatest._

class MainSpec extends FlatSpec with Matchers {

  val emptyArgs: Array[String] = Array()
  val emptyArguments           = Arguments()

  "Parser defined in Main" should "parse and validate arguments" in {

    // to suppress parsing errors
    Console.withErr(new java.io.ByteArrayOutputStream()) {

      parser.parse(emptyArgs, emptyArguments) shouldBe None

      parser.parse(testCase1.args, emptyArguments) shouldBe Some(
        testCase1.arguments
      )

      parser.parse(testCase2.args, emptyArguments) shouldBe Some(
        testCase2.arguments
      )

      parser.parse(testCase3.args, emptyArguments) shouldBe Some(
        testCase3.arguments
      )

      (-10 to 137).foreach { pebbleArg =>
        val result = parser.parse(Array(pebbleArg.toString), emptyArguments)
        if (pebbleArg >= 27 && pebbleArg <= 127) {
          result shouldBe Some(Arguments(Some(pebbleArg)))
        } else {
          result shouldBe None
        }
      }

      (-10 to 10).foreach { minutesArg =>
        val result = parser.parse(Array("30", minutesArg.toString), emptyArguments)
        if (minutesArg > 0) {
          result shouldBe Some(Arguments(Some(30), Some(minutesArg)))
        } else {
          result shouldBe None
        }
      }
    }

  }

  "Main object performed simulation" should "give correct results" in {

    Main.resultForArguments(Main.assignmentEmptyPebbleClock, Arguments()) shouldBe
      Left("Pebbles to deposit field is required.")

    Main.resultForArguments(testCase1.createPebbleClock(), testCase1.arguments) shouldBe
      Right(testCase1.expectedOutput)

    Main.resultForArguments(testCase2.createPebbleClock(), testCase2.arguments) shouldBe
      Right(testCase2.expectedOutput)

    Main.resultForArguments(testCase3.createPebbleClock(), testCase3.arguments) shouldBe
      Right(testCase3.expectedOutput)

  }

}
