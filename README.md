## Pebble Clock Assignment

### Try It

You can try it from sbt console:

```
test
[info] All tests passed.

run 123
[info] Running assignment.Main 123
123 pebbles cycle after 108855 days.
[success] Total time: 5 s...
```

After optimizations program performs `~31M` iterations per second on my notebook.
Changing implementation to mutable wasn't that hard but some unit tests were this time abandoned.

Although the assignment states that the clock specification is the fixed one, the implementation of `PebbleClock` class is a generic
and allows for even more peculiar setups.

### Assignment description

Movement has long been used to measure time. For example, the pebble clock is a simple device which keeps track of the passing minutes by moving pebble-bearings. Each minute, a rotating arm removes a pebble bearing from the queue at the bottom, raises it to the top of the clock and deposits it on a track leading to indicators displaying minutes, five-minutes and hours. These indicators display the time between 1:00 and 12:59, but without 'a.m.' or 'p.m.' indicators. Thus 2 pebbles in the minute indicator, 6 pebbles in the five-minute indicator and 5 pebbles in the hour indicator displays the time 5:32.

Unfortunately, most commercially available pebble clocks do not incorporate a date indication, although this would be simple to do with the addition of further carry and indicator tracks. However, all is not lost! As the pebbles migrate through the mechanism of the clock, the order they are found in can be used to determine the time elapsed since the clock had some specific ordering. The length of time which can be measured is limited because the orderings of the pebbles eventually begin to repeat. Your program must compute the time before repetition, which varies according to the total number of pebbles present.

#### Operation of the Pebble Clock

Every minute, the least recently used pebble is removed from the queue of pebbles at the bottom of the clock, elevated, then deposited on the minute indicator track, which is able to hold four pebbles. When a fifth pebble rolls on to the minute indicator track, its weight causes the track to tilt. The four pebbles already on the track run back down to join the queue of pebbles waiting at the bottom in reverse order of their original addition to the minutes track. The fifth pebble, which caused the tilt, rolls on down to the five-minute indicator track. This track holds eleven pebbles. The twelfth pebble carried over from the minutes causes the five-minute track to tilt, returning the eleven pebbles to the queue, again in reverse order of their addition. The twelfth pebble rolls down to the hour indicator. The hour indicator also holds eleven pebbles, but has one extra fixed pebble which is always present so that counting the pebbles in the hour indicator will yield an hour in the range one to twelve. The twelfth pebble carried over from the five-minute indicator causes the hour indicator to tilt, returning the eleven free pebbles to the queue, in reverse order, before the twelfth pebble itself also returns to the queue.

#### Guidelines

The exercise may be completed in the language of your choice. 

No permutation or LCM algorithms are allowed.  A full simulation is required. Please ensure that your code moves each pebble.

#### Implementation

Valid numbers are in the range 27 to 127.

Clocks must support two modes of computation.

The first mode takes a single parameter specifying the number of pebbles and reports the number of pebbles given in the input and the number of days (24-hour periods) which elapse before the clock returns to its initial ordering.

Sample Input
 
```
30
45
```
Output for the Sample Input
  
```
30 pebbles cycle after 15 days.
45 pebbles cycle after 378 days.
```
The second mode takes two parameters, the number of pebbles and the number of minutes to run for.  If the number of minutes is specified, the clock must run to the number of minutes and report the state of the tracks at that point in a JSON format.

Sample Input
  
```
30 325
```

Output for the Sample Input
  
```json
{"Min":[],"FiveMin":[22,13,25,3,7],"Hour":[6,12,17,4,15],"Main":[11,5,26,18,2,30,19,8,24,10,29,20,16,21,28,1,23,14,27,9]}
```
