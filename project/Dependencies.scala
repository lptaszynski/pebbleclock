import sbt._

object Dependencies {

  lazy val scalaTest = "org.scalatest"    %% "scalatest" % "3.0.5"
  lazy val scopt     = "com.github.scopt" %% "scopt"     % "3.7.1"
  lazy val circe = Seq(
    "io.circe" %% "circe-core",
    "io.circe" %% "circe-generic",
    "io.circe" %% "circe-parser"
  ).map(_ % circeVersion)
  val circeVersion = "0.11.1"
}
