import Dependencies._

ThisBuild / scalaVersion := "2.12.8"
ThisBuild / version := "1.0.0"

lazy val root = (project in file("."))
  .settings(
    name := "PebbleClock",
    libraryDependencies += scalaTest % Test,
    libraryDependencies += scopt,
    libraryDependencies ++= circe
  )
